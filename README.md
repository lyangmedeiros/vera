## VERY EASY ROBOTIC ARM - VERA
---
Conjunto e bibliotecas e softwares em *Python* e *C++* para controle de braços robóticos baseados na plataforma *Arduino*.

Desenvolvido por membros e voluntários do [Capítulo Estudantil IEEE RAS UFCG](https://www.facebook.com/rasufcg/)

---
**vision-sys.py** - Python2

Software para identificação e localização de objetos.

Módulos necessários:

 - OpenCV -  (`pip install opencv-python`)
 - MatplotLib - (`pip install matplotlib`)
 - Pillow - (`pip install Pillow`)
 - Numpy - (`pip install numpy`)